﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.Win32;
using PET.Model;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading;
using System.Windows.Threading;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using System.Windows.Controls.DataVisualization;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.DataVisualization.Charting;
using LiveCharts.Configurations;

namespace PET
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        OpenFileDialog txtDlg;
        String txtLocation = "";

        //initialize the first welllog object
        ObservableCollection<WellLogFirst> firstwelllogs = new ObservableCollection<WellLogFirst>();
        List<WellLogFirst> firstwelllogslist = new List<WellLogFirst>();

        //ChartValues<Double> caliTestDts = new ChartValues<double>();
        //List<KeyValuePair<Double, Double>> caliTestDts = new List<KeyValuePair<Double, Double>>();
        ChartValues<WellLogFirst> caliTestDts = new ChartValues<WellLogFirst>();

        public ChartValues<double> Values1 { get; set; }
        public SeriesCollection SeriesCollection { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            var mapper = Mappers.Xy<WellLogFirst>().X(v => v.Rhob).Y(v => v.Depth);
            SeriesCollection = new SeriesCollection(mapper)
            {
                new VerticalLineSeries
                {
                    Title = "Series 1",
                    Values = new ChartValues<WellLogFirst> ()
                }
                
            };
            
            DataContext = this;
            
        }

        private void LoadLineChartData()
        {

            //((System.Windows.Controls.DataVisualization.Charting.LineSeries)mcChart.Series[0]).ItemsSource =
            //    new KeyValuePair<DateTime, int>[]{
            //new KeyValuePair<DateTime, int>(DateTime.Now, 100),
            //new KeyValuePair<DateTime, int>(DateTime.Now.AddMonths(1), 130),
            //new KeyValuePair<DateTime, int>(DateTime.Now.AddMonths(2), 150),
            //new KeyValuePair<DateTime, int>(DateTime.Now.AddMonths(3), 125),
            //new KeyValuePair<DateTime, int>(DateTime.Now.AddMonths(4), 155) };
        }

        private void browseTxt_click(object sender, RoutedEventArgs e)
        {
            //OpenFileDialog initialization
            txtDlg = new OpenFileDialog();
            txtDlg.Filter = "Text Files|*.txt";
            txtDlg.Title = "Text files containing message";
            txtDlg.ShowDialog();
            //Start the appending to file chosen from the file dialog
            txtLocation = txtDlg.FileName;
            txtLocation_tb.Text = txtLocation;

            Thread th = new Thread(analyseSecondLog);
            th.Start();
            
            
        }

        private void loopnos(object obj)
        {
            LoadLineChartData();
        }

        private void analyseSecondLog(object obj)
        {
            welllogsDG.ItemsSource = null;
            //initialize a temp firstwelllog object
            WellLogFirst tempFirstLogObj = new WellLogFirst();
            //continue and read the file
            //calculate as you read
            FileStream theFile = new FileStream(txtLocation, FileMode.Open);
            StreamReader sr = new StreamReader(theFile);

            sr.ReadLine();//reading out the header
            string line = sr.ReadLine();

            int counter = 0;
            //run a loop through the rows
            while (line!=null)
            {
                string[] values = line.Split(new char[] { ' ', '\t' });//split the columns in the rows
                //parsing the doubles into the object
                tempFirstLogObj.Depth = (Double.Parse(values[0].Trim()) == -999.25) ? 0.0 : Double.Parse(values[0].Trim());
                tempFirstLogObj.Nphi = (Double.Parse(values[1].Trim()) == -999.25) ? 0.0 : Double.Parse(values[1].Trim());
                tempFirstLogObj.Drho = (Double.Parse(values[2].Trim()) == -999.25) ? 0.0 : Double.Parse(values[2].Trim());
                tempFirstLogObj.Gr = (Double.Parse(values[3].Trim()) == -999.25) ? 0.0 : Double.Parse(values[3].Trim());
                tempFirstLogObj.Rhob = (Double.Parse(values[4].Trim()) == -999.25) ? 0.0 : Double.Parse(values[4].Trim());
                tempFirstLogObj.Cali = (Double.Parse(values[5].Trim()) == -999.25) ? 0.0 : Double.Parse(values[5].Trim());
                tempFirstLogObj.Sn = (Double.Parse(values[6].Trim()) == -999.25) ? 0.0 : Double.Parse(values[6].Trim());
                tempFirstLogObj.Ild = (Double.Parse(values[7].Trim()) == -999.25) ? 0.0 : Double.Parse(values[7].Trim());
                tempFirstLogObj.Sp = (Double.Parse(values[8].Trim()) == -999.25) ? 0.0 : Double.Parse(values[8].Trim());
                tempFirstLogObj.Dt = (Double.Parse(values[9].Trim()) == -999.25) ? 0.0 : Double.Parse(values[9].Trim());
                //run your calculations 



                //caliTestDts.Add(new KeyValuePair<Double, Double>(tempFirstLogObj.Depth, tempFirstLogObj.Ild));
                if (counter%20==0)
                {
                    caliTestDts.Add(tempFirstLogObj);
                }
                
                firstwelllogslist.Add(tempFirstLogObj);
                tempFirstLogObj = new WellLogFirst();//initialize anew for the next row of data  
                line = sr.ReadLine();
                counter++;
                //SeriesCollection[0].Values.Add(tempFirstLogObj.Dt);
            }
            
            this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate()
            {
                welllogsDG.ItemsSource = firstwelllogslist;
                //((System.Windows.Controls.DataVisualization.Charting.LineSeries)mcChart.Series[0]).ItemsSource =caliTestDts;
                SeriesCollection[0].Values = caliTestDts;
                //SeriesCollection[0].Values.Add(caliTestDts[0]);
            });

            sr.Close();
            theFile.Close();
        }

        private void analyseFirstLog(object obj)
        {
            //initialize a temp firstwelllog object
            WellLogFirst tempFirstLogObj = new WellLogFirst();
            //continue and read the file
            //calculate as you read
            FileStream theFile = new FileStream(txtLocation, FileMode.Open);
            BinaryReader sr = new BinaryReader(theFile);//using binary reader to read the doubles
            Double tempDouble=sr.ReadDouble();

            //initialize the temp counter to manage reading columns for a single line, thereby managing lines
            int tempCounter = 0;
            while(true){
                try
                {
                    switch (tempCounter)
                    {
                        case 0:
                            tempFirstLogObj.Depth = tempDouble;
                            break;
                        case 1:
                            tempFirstLogObj.Nphi = tempDouble;
                            break;
                        case 2:
                            tempFirstLogObj.Drho = tempDouble;
                            break;
                        case 3:
                            tempFirstLogObj.Gr = tempDouble;
                            break;
                        case 4:
                            tempFirstLogObj.Rhob = tempDouble;
                            break;
                        case 5:
                            tempFirstLogObj.Cali = tempDouble;
                            break;
                        case 6:
                            tempFirstLogObj.Sn = tempDouble;
                            break;
                        case 7:
                            tempFirstLogObj.Ild = tempDouble;
                            break;
                        case 8:
                            tempFirstLogObj.Sp = tempDouble;
                            break;
                        case 9://end of the line, gotten to the last column
                            tempFirstLogObj.Dt = tempDouble;
                            tempCounter = 0;
                            //run your calculations here


                            this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate()
                            {
                                //push to the collection
                                firstwelllogs.Add(tempFirstLogObj);
                                tempFirstLogObj = new WellLogFirst();//initialize anew for the next row of data   
                            });
                            break;
                        default:
                            break;
                    }
                    tempDouble = sr.ReadDouble();
                    tempCounter++;

                }
                catch (EndOfStreamException e)
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate()
                    {
                        MessageBox.Show(e.Message);
                    });
                    break;
                }
                
            }
        }
    }
}
