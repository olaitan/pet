﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PET.Model
{
    class WellLogFirst
    {
        public Double Depth { get; set; }
        public Double Nphi { get; set; }
        public Double Drho { get; set; }
        public Double Gr { get; set; }
        public Double Rhob { get; set; }
        public Double Cali { get; set; }
        public Double Sn { get; set; }
        public Double Ild { get; set; }
        public Double Sp { get; set; }
        public Double Dt { get; set; }
    }
}
